# Flatten an array in python
#
#  [[1,2,[3]],4] -> [1,2,3,4]
#
#
# Implemented with a recursive generator function
# and using yield from syntax from python 3.3
#

from collections import Iterable

test_data = [[1, 2, [3]], 4, [3, [5, 7, 5, [5, 2]]]]


def flatten(data):
    """
    Recursive generator function for any iterable including nested ones
    """
    for x in data:
        if isinstance(x, Iterable) and not isinstance(x, (str, bytes)):
            # Delegate generation to the last one
            # (avoids too many context switches)
            yield from flatten(x)
        else:
            # Actual value
            yield x


if __name__ == "__main__":
    print(str(list(flatten(test_data))))
