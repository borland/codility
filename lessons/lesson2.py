#
#
# OddOccurrencesInArray
#
# Find value that occurs in odd number of elements.
#
# A non-empty zero-indexed array A consisting of N integers is given. The array contains an odd number of elements,
# and each element of the array can be paired with another element that has the same value, except for one element
# that is left unpaired.
#
# For example, in array A such that:
#  A[0] = 9  A[1] = 3  A[2] = 9
#  A[3] = 3  A[4] = 9  A[5] = 7
#  A[6] = 9
#
#        the elements at indexes 0 and 2 have value 9,
#        the elements at indexes 1 and 3 have value 3,
#        the elements at indexes 4 and 6 have value 9,
#        the element at index 5 has value 7 and is unpaired.
#
# Write a function:
#
#   def solution(A)
#
# that, given an array A consisting of N integers fulfilling the above conditions, returns the value of the unpaired
# element.
#
# For example, given array A such that:
#  A[0] = 9  A[1] = 3  A[2] = 9
#  A[3] = 3  A[4] = 9  A[5] = 7
#  A[6] = 9
#
# the function should return 7, as explained in the example above.
#
# Assume that:
#
#        N is an odd integer within the range [1..1,000,000];
#        each element of array A is an integer within the range [1..1,000,000,000];
#        all but one of the values in A occur an even number of times.
#
# Complexity:
#
# expected worst-case time complexity is O(N); expected worst-case space complexity is O(1), beyond input storage (
# not counting the storage required for input arguments).
#
# Elements of input arrays can be modified.


def odd_pairs1(A):
    """ Find paired numbers and return the unpaired one"""

    paired = []

    for i, item in enumerate(A):
        for j, item2 in enumerate(A):
            if i == j:
                continue
            else:
                if item == item2:
                    paired.append(item)
                    break

    return [x for x in A if x not in paired][0]


def odd_pairs(A):
    """ Find paired numbers and return the unpaired one"""

    A.sort()
    n = len(A)
    i = 0

    while i < n:

        try:
            if A[i] == A[i+1]:
                i += 2
            else:
                return A[i]
        except:
            return A[n-1]

#
# Task description
#
# A zero-indexed array A consisting of N integers is given. Rotation of the array means that each element is shifted
# right by one index, and the last element of the array is also moved to the first place.
#
# For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7]. The goal is to rotate array A K times;
# that is, each element of A will be shifted to the right by K indexes.
#
# Write a function:
#
#    def solution(A, K)
#
# that, given a zero-indexed array A consisting of N integers and an integer K, returns the array A rotated K times.
#
# For example, given array A = [3, 8, 9, 7, 6] and K = 3, the function should return [9, 7, 6, 3, 8].
#
# Assume that:
#
#        N and K are integers within the range [0..100];
#        each element of array A is an integer within the range [−1,000..1,000].
#
# In your solution, focus on correctness. The performance of your solution will not be the focus of the assessment.
#


def rotating_array(A, K):
    #print(str(K))
    #print(str(len(A)))
    #print(str(K/len(A)))
    #print(str(A[:-K]))
    #print(str(A[-K:]))

    if K < len(A):
        return A[-K:] + A[:-K]
    else:
        try:
            return A[-K % len(A):] + A[:-K % len(A)]
        except ZeroDivisionError:
            return A

if __name__ == "__main__":
    #print(rotating_array([3, 8, 9, 7, 6], 3))
    print(rotating_array([1, 1, 2, 3, 5], 42))
    print(rotating_array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100]
, 100))
    #print(odd_pairs([9, 3, 9, 3, 9, 7, 9]))
    #print(odd_pairs([9, 3, 9, 3, 9, 7, 9, 3, 3, 7, 1]))
    #print(odd_pairs([9, 3, 9, 3, 9, 7, 7, 9, 4, 4, 8]))
    #print(odd_pairs([24]))