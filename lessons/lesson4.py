# MissingInteger
# Find the smallest positive integer that does not occur in a given sequence
#
#


def missing_integer(A):
    print('\ninput: ' + str(A))
    A.sort()
    missing = 1

    for elem in A:
        if elem == missing:
            missing += 1

    return missing


#
# PermCheck
# Check whether array A is a permutation.
#

def permutation_check(A):
    print('\ninput: ' + str(A))
    A.sort()
    print('sorted: ' + str(A))

    for i in range(0, len(A)):
        print(i+1, A[i])
        if (i+1) != A[i]:
            return 0

    return 1


def frog_river_one(X, A):
    print('\nA: ' + str(A))
    print('X: ' + str(X))

    for i in range(0, len(A)):
        if A[i] == X:
            return i

    return -1



if __name__ == '__main__':
    #print(missing_integer([1, 3, 6, 4, 1, 2]))
    #print(missing_integer([1, 2, 3]))
    #print(permutation_check([4, 1, 3, 2]))
    #print(permutation_check([4, 1, 3]))
    #print(permutation_check([2]))
    print(frog_river_one(5, [1, 3, 1, 4, 2, 3, 5, 4]))
