#
# BinaryGap
#
# Find longest sequence of zeros in binary representation of an integer.
# -----------------
#
# A binary gap within a positive integer N is any maximal sequence of consecutive zeros that is surrounded by ones at
#  both ends in the binary representation of N.
#
# For example, number 9 has binary representation 1001 and contains a binary gap of length 2. The number 529 has
# binary representation 1000010001 and contains two binary gaps: one of length 4 and one of length 3. The number 20
# has binary representation 10100 and contains one binary gap of length 1. The number 15 has binary representation
# 1111 and has no binary gaps.
#
# Write a function:
#
#    def solution(N)
#
# that, given a positive integer N, returns the length of its longest binary gap. The function should return 0 if N
# doesn't contain a binary gap.
#
# For example, given N = 1041 the function should return 5, because N has binary representation 10000010001 and so
# its longest binary gap is of length 5.
#
# Assume that:
#
#        N is an integer within the range [1..2,147,483,647].
#
# Complexity:
#
#        expected worst-case time complexity is O(log(N));
#        expected worst-case space complexity is O(1).
#


def binary_gap(N):
    """

    Find longest sequence of zeros in binary representation of an integer

    :param N: input
    :return: largest gap
    """

    # Convert number to a binary string representation
    binary = "{0:b}".format(N)

    # Store the different gaps found in this number
    one_list = []
    stored_gaps = []

    # Need to count the amount of contiguous zeroes between ones
    for index, elem in enumerate(binary):
        if elem == '1':
            one_list.append(index)

    if 1 < len(one_list) < len(binary):
        reversed_ones = list(reversed(one_list))
        for i, one in enumerate(reversed_ones):
            try:
                if i == 0:
                    # Need to add 1 to the index, otherwise it counts the first 1 towards the zero count
                    stored_gaps.append((one - reversed_ones[i + 1]) - (i+1))
                else:
                    stored_gaps.append((one - reversed_ones[i + 1]) - i)

            except IndexError as e:
                # Ignore last one
                pass

        return sorted(reversed(stored_gaps))[-1]
    else:
        return 0


if __name__ == "__main__":
    print(binary_gap(1041)) # 5
    print(binary_gap(529)) # 4
    print(binary_gap(9)) # 2
    print(binary_gap(20)) # 1
    print(binary_gap(15)) # 0